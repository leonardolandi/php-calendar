![Screenshot](screenshot.png)

<h3>Usage:</h3>
Include the code inside your HTML or PHP file with php:
<pre>
include("./calendar.php");
</pre>

<h3>Highlighting days:</h3>
You can set explicit rules in order to highlight some days, according to this schema:
<table><tbody>
<tr>
<td colspan=2><h5>Days</h5></td>
<td colspan=2><h5>Selector</h5></td>
<td colspan=2><h5>Months</h5></td></tr>
<tr>
<td>1<br>2<br>4<br>8<br>16<br>32<br>64</td>
<td>Monday<br>Tuesday<br>Wednesday<br>Thursday<br>Friday<br>Saturday<br>Sunday</td>
<td>1<br>2<br>4<br>8<br>16<br>32<br>64<br>128</td>
<td>All days<br>The first of the month<br>The second of the month<br>The third of the month<br>The fourth of the month<br>The fifth of the month<br>The last of the month<br>The second-last of the month</td>
<td>1<br>2<br>4<br>8<br>16<br>32<br>64<br>128<br>256<br>512<br>1024<br>2048</td>
<td>January<br>February<br>March<br>April<br>May<br>June<br>July<br>August<br>September<br>October<br>November<br>December</td>
</tr>
</tbody></table>

For example, if you want to specify the rule <i>"The first Mondays and Wednesdays of September and October"</i>, this rule will be:
- <b>Days</b>: 1 + 4 = 5 (Monday + Wednesday)
- <b>Selector</b>: 2 (The first of the month)
- <b>Month</b>: 256 + 512 = 768 (September + October)

Each rule must be declared inside a php array this way:
<pre>
$rule = array("days" => 5, "selector" => 2, "months" => 768);
</pre>
All rules must be specified in a php array called <code>$CAL_rules</code> before including the file. For example:
<pre>
$rule1 = array("days" => 5, "selector" => 2, "months" => 768);
$rule2 = array("days" => 64, "selector" => 64, "months" => 128);
$CAL_rules = array($rule1, $rule2);
include("./calendar.php");
</pre>
