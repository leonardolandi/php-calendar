<!--calendar.php

Copyright (C) 2016 Leonardo Landi

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.-->


<!--Include 'calendar.php' into your external HTML body

If you want to specify which days are gonna be selected
declare a php array named '$CAL_rules' before including
this file, according to the schema specified into README.md-->


<!--PHP $CAL_rules validation-->
<?php
	
	$CAL_error = False;
	
	if ($CAL_rules !== NULL && gettype($CAL_rules) != "array") { $CAL_error = False; }
	else if ($CAL_rules !== NULL) {
		foreach ($CAL_rules as &$rule) {
			// Validate "days". We assume a given value if key is not set or out of range
			if (!isset($rule["days"])) { $rule["days"] = 127; }
			else if (gettype($rule["days"]) != "integer") { $CAL_error = True; break; }
			else if ($rule["days"] < 1) { $rule["days"] = 1; }
			else if ($rule["days"] > 127) { $rule["days"] = 127; }
			// Validate "selector". We assume a given value if key is not set or out of range
			if (!isset($rule["selector"])) { $rule["selector"] = 255; }
			else if (gettype($rule["selector"]) != "integer") { $CAL_error = True; break; }
			else if ($rule["selector"] < 1) { $rule["selector"] = 1; }
			else if ($rule["selector"] > 255) { $rule["selector"] = 255; }
			// Validate "months". We assume a given value if key is not set or out of range
			if (!isset($rule["months"])) { $rule["months"] = 4095; }
			else if (gettype($rule["months"]) != "integer") { $CAL_error = True; break; }
			else if ($rule["months"] < 1) { $rule["months"] = 1; }
			else if ($rule["months"] > 4095) { $rule["months"] = 4095; }
		}
	}	
	
?>


<!--HTML side-->
<table id=CAL_main></tbody>

	<tr id=CAL_toprow>
		<td><button id=CAL_left><</button></td>
		<td colspan=5 id=CAL_month></td>
		<td><button id=CAL_right>></button></td>
	</tr>
	
	<tr id=CAL_days>
		<td>M</td>
		<td>T</td>
		<td>W</td>
		<td>T</td>
		<td>F</td>
		<td>S</td>
		<td>S</td>
	</tr>
	
	<?php
		for ($i=0; $i<6; $i++) {
			echo "<tr>";
			for ($j=0; $j<7; $j++) { echo "<td class=CAL_cell id=CAL_" . $i . "_" . $j . "></td>"; }
			echo "</tr>";
		}
	?>
	
	<tr><td colspan=7 id=CAL_error
		<?php
			if ($CAL_error) { echo ">Calendar rules are not well defined"; }
			else { echo " style=display:none>"; }
		?>
	</td></tr>
	
</tbody></table>


<!--JAVASCRIPT functions-->
<script>

	function CAL_fill_table(month,year) {
		var months = ["January","February","March","April","May","June","July","August","September","October","November","December"];
		// Insert the title
		document.getElementById("CAL_month").innerHTML = months[month] + " " + year;
		// Update left arrow
		var month_before = (month + 11) % 12, year_before = year;
		if (month_before == 11) { year_before -= 1 }
		document.getElementById("CAL_left").onclick = function() { CAL_fill_table(month_before,year_before) }
		// Update right arrow
		var month_next = month + 1, year_next = year;
		if (month_next == 12) { month_next = 0; year_next += 1 }
		document.getElementById("CAL_right").onclick = function() { CAL_fill_table(month_next,year_next) }
		// Find the day at position (0,0)
		var first = new Date(year,month,1,0,0,0,0);
		var position = (first.getDay() + 6) % 7;
		while (position > 0) {
			first = new Date(first.getTime() - 86400000);
			position = (first.getDay() + 6) % 7;
		}
		// Fill the table cells
		while (position < 42) {
			var column = position % 7;
			var row = (position - column) / 7;
			var cell = document.getElementById("CAL_" + row + "_" + column);
			cell.innerHTML = first.getDate();
			if (first.getMonth() == month) { cell.style.fontWeight = "bolder" }
			else { cell.style.fontWeight = "normal" }
			if (CAL_verify(first)) { cell.classList.add("CAL_selected") }
			else { cell.classList.remove("CAL_selected") }
			first = new Date(first.getTime() + 90000000); // avoid DST
			first.setHours(0);
			position += 1;
		}
	}
	
	// Function that verifies if a given day satisfies the given rules
	function CAL_verify(date) {
		var rules = <?php echo json_encode($CAL_rules); ?>;
		for (k in rules) {
			var rule = rules[k];
			var days = [], selector = [], months = [];
			// Fill the "days" array
			for (var i=6; i>-1; i--) {
				var power = Math.pow(2,i);
				if (rule["days"] >= power) { days.unshift(true); rule["days"] -= power }
				else { days.unshift(false) }
			}
			// Fill the "selector" array
			for (var i=7; i>-1; i--) {
				var power = Math.pow(2,i);
				if (rule["selector"] >= power) { selector.unshift(true); rule["selector"] -= power }
				else { selector.unshift(false) }
			}
			// Fill the "months" array
			for (var i=11; i>-1; i--) {
				var power = Math.pow(2,i);
				if (rule["months"] >= power) { months.unshift(true); rule["months"] -= power }
				else { months.unshift(false) }
			}
			// Verifies if the day is going to be selected
			if (!months[date.getMonth()]) { continue }
			if (!days[(date.getDay() + 6) % 7]) { continue }
			if (selector[0]) { return true }
			var day = date.getDate();
			if (selector[1] && day < 8) { return true }
			if (selector[2] && day > 7 && day < 15) { return true }
			if (selector[3] && day > 14 && day < 22) { return true }
			if (selector[4] && day > 21 && day < 29) { return true }
			if (selector[5] && day > 28 && day < 32) { return true }
			var month_next = date.getMonth() + 1, year_next = date.getFullYear();
			if (month_next == 12) { month_next = 0; year_next += 1 }
			var last = (new Date((new Date(year_next,month_next,1,0,0,0,0)).getTime() - 82800000)).getDate(); // avoid DST
			if (selector[6] && day > last - 7) { return true }
			if (selector[7] && day > last - 14 && day < last - 7) { return true }
		}
		return false;
	}
	
	// Launch the function
	CAL_fill_table((new Date()).getMonth(),(new Date()).getFullYear());
	
</script>


<!--CSS style-->
<style>
	
	@import url('https://fonts.googleapis.com/css?family=Exo:500');
	
	#CAL_main {
		background-color: #e9eff0;
		border: 1px solid #173039;
		border-spacing: 0;
		font-family: "Exo", sans-serif;
		font-size: small;
	}
	
	#CAL_main tr td hr {
		margin: 0;
	}
	
	#CAL_toprow {
		background-color: #173039;
	}
	
	#CAL_toprow td {
		color: white;
		font-weight: bolder;
		text-align: center;
	}
	
	#CAL_toprow td button {
		background: none;
		border: none;
		color: white;
		font-weight: bolder;
		text-align: center;
	}
	
	#CAL_days {
		background-color: #00525f;
		color: white;
		font-weight: bolder;
		text-align: center;
	}
	
	#CAL_error {
		background-color: #ff615d;
		text-align: center;
	}
	
	.CAL_cell {
		color: #173039;
		padding: 3px 8px;
		text-align: center;
	}
	
	.CAL_selected {
		background-color: #ff615d;
	}
	
</style>
